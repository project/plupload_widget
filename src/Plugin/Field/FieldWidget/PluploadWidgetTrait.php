<?php

namespace Drupal\plupload_widget\Plugin\Field\FieldWidget;

use Drupal\Component\Utility\Bytes;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Form\FormStateInterface;

/**
 * Shared functionality between all widgets.
 */
trait PluploadWidgetTrait {

  /**
   * Get the optimum chunk size.
   *
   * This is the maximum upload size that PHP can handle. The lesser of
   * upload_max_filesize/post_max_size - we have to remove 2Kb from this to
   * account for averhead of the multipart/form-data and boundary information.
   *
   * @return float|int
   *   The optimum chunk size
   */
  public function getChunkSize() {
    $sizes = [
      Bytes::toNumber(ini_get('post_max_size')),
      Bytes::toNumber(ini_get('upload_max_filesize')),
    ];
    return min($sizes) - 2048;
  }

  /**
   * Returns the maximum configured file size for the Field stroage in Bytes.
   *
   * @return double|int
   *   The maximum file size
   */
  public function getMaxFileSize() {
    // We don't care about PHP's max post
    // or upload file size because we use
    // plupload.
    $size = $this->getFieldSetting('max_filesize');
    $size = Bytes::toNumber($size);
    return $size;
  }

  /**
   * {@inheritdoc}
   *
   * Replace the description text provided by core because max file size is
   * wrong (capped by php_ini settings).
   */
  protected function formMultipleElements(FieldItemListInterface $items, array &$form, FormStateInterface $form_state) {
    $elements = parent::formMultipleElements($items, $form, $form_state);

    $cardinality = $elements[0]['#cardinality'];
    $current_items = count($elements[0]['#default_value']['fids']);

    // Need to replace the description when upload form is available.
    if ($cardinality == -1 || $current_items < $cardinality) {
      $max_size = $this->getMaxFileSize();

      // Prepare the file upload help with a correct max file size.
      $file_upload_help = [
        '#theme' => 'file_upload_help',
        '#description' => $items->getFieldDefinition()->getDescription(),
        '#upload_validators' => $max_size == 0 ? [] : ['file_validate_size' => [$max_size]],
        '#cardinality' => $cardinality,
      ];

      if ($cardinality === 1) {
        $elements[0]['#description'] = $file_upload_help;
      }
      else {
        $elements['#file_upload_description'] = $file_upload_help;
      }
    }

    return $elements;
  }

}
