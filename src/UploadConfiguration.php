<?php

namespace Drupal\plupload_widget;

/**
 * Summary of UploadConfiguration.
 */
class UploadConfiguration {

  /**
   * Maximum number of files to upload.
   *
   * @var int
   */
  // @codingStandardsIgnoreStart
  public $max_files;
  // @codingStandardsIgnoreEnd

  /**
   * A list of validators to apply to uploaded files.
   *
   * @var string[]
   */
  public $validators;

  /**
   * Size of each portion of submitted data.
   *
   * @var string
   */
  // @codingStandardsIgnoreStart
  public $chunk_size;
  // @codingStandardsIgnoreEnd

  /**
   * Maximum size of uploaded files.
   *
   * @var string
   */
  // @codingStandardsIgnoreStart
  public $max_size;
  // @codingStandardsIgnoreEnd

  /**
   * Number of files for the field.
   *
   * @var int
   */
  public $cardinality;

  /**
   * Location where files should be uploaded.
   *
   * @var string
   */
  // @codingStandardsIgnoreStart
  public $upload_location;
  // @codingStandardsIgnoreEnd

}
