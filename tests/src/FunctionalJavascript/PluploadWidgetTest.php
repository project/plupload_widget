<?php

declare(strict_types=1);

namespace Drupal\Tests\plupload_widget\FunctionalJavascript;

use Drupal\Component\Utility\DeprecationHelper;
use Drupal\FunctionalJavascriptTests\WebDriverTestBase;
use Drupal\Tests\field_ui\Traits\FieldUiTestTrait;
use Drupal\Tests\file\Functional\FileFieldCreationTrait;
use Drupal\Tests\image\Kernel\ImageFieldCreationTrait;
use Drupal\Tests\TestFileCreationTrait;

/**
 * Tests the file field widget, single and multivalued, using AJAX upload.
 *
 * @group file
 */
class PluploadWidgetTest extends WebDriverTestBase {

  use FieldUiTestTrait;
  use FileFieldCreationTrait;
  use ImageFieldCreationTrait;
  use TestFileCreationTrait;

  /**
   * A user with administration permissions.
   *
   * @var \Drupal\user\UserInterface
   */
  protected $adminUser;

  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    'node',
    'file',
    'image',
    'field_ui',
    'plupload_widget',
  ];

  /**
   * {@inheritdoc}
   */
  protected $defaultTheme = 'stark';

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();
    $this->adminUser = $this->drupalCreateUser([
      'bypass node access',
    ]);
    $this->drupalLogin($this->adminUser);
    $this->drupalCreateContentType(['type' => 'article', 'name' => 'Article']);
  }

  /**
   * Tests uploading and remove buttons for a single-valued Plupload file field.
   */
  public function testFileWidget() {
    $type_name = 'article';
    $field_name = 'test_file_field_1';
    $cardinality = 1;
    $this->createFileField($field_name, 'node', $type_name, ['cardinality' => $cardinality]);
    $this->setPluploadFileWidget($field_name, 'node', $type_name);

    $page = $this->getSession()->getPage();
    $assert_session = $this->assertSession();

    // Create a node with a valid file extension.
    $test_file = current($this->getTestFiles('text'));
    $test_file_path = \Drupal::service('file_system')
      ->realpath($test_file->uri);

    $this->drupalGet("node/add/$type_name");

    $page->findField('title[0][value]')->setValue($this->randomString());

    $assert_session->buttonNotExists('Remove');
    $this->addFile($test_file_path, '#edit-test-file-field-1-0-upload--2');
    $assert_session->waitForText('Uploaded 1/1 files');

    $assert_session->waitForButton('Remove');
    $assert_session->pageTextNotContains(' Upload files');

    $page->pressButton('Save');
    $assert_session->statusMessageContains(' has been created.');

    $this->drupalGet("node/1/edit");
    $page->hasContent($test_file->name);
  }

  /**
   * Tests uploading and remove buttons for a Plupload image field.
   */
  public function testImageWidget() {
    $type_name = 'article';
    $field_name = 'test_image_field_1';
    $cardinality = 1;

    if (class_exists(DeprecationHelper::class)) {
      DeprecationHelper::backwardsCompatibleCall(\Drupal::VERSION, '10.3',
        fn() => $this->createImageField($field_name, 'node', $type_name, ['cardinality' => $cardinality], ['alt_field_required' => 0]),
        fn() => $this->createImageField($field_name, $type_name, ['cardinality' => $cardinality], ['alt_field_required' => 0]));
    }
    // Drupal 9.5.
    else {
      $this->createImageField($field_name, $type_name, ['cardinality' => $cardinality], ['alt_field_required' => 0]);
    }
    $this->setPluploadImageWidget($field_name, 'node', $type_name);

    $page = $this->getSession()->getPage();
    $assert_session = $this->assertSession();

    // Create a node with a valid file extension.
    $test_file = current($this->getTestFiles('image'));
    $test_file_path = \Drupal::service('file_system')
      ->realpath($test_file->uri);

    $this->drupalGet("node/add/$type_name");

    $page->findField('title[0][value]')->setValue($this->randomString());

    $this->addFile($test_file_path, '#edit-test-image-field-1-0-upload--2');
    $assert_session->waitForText('Uploaded 1/1 files');

    $assert_session->waitForButton('Remove');
    $assert_session->pageTextNotContains(' Upload files');

    $page->pressButton('Save');
    $assert_session->statusMessageContains(' has been created.');

    $this->drupalGet("node/1/edit");
    $page->hasContent($test_file->name);
  }

  /**
   * Update file field widget to use Plupload.
   *
   * @param string $name
   *   The name of the field.
   * @param string $entity_type
   *   The entity type.
   * @param string $bundle
   *   The bundle.
   */
  protected function setPluploadFileWidget(string $name, string $entity_type, string $bundle) {
    /** @var \Drupal\Core\Entity\EntityDisplayRepositoryInterface $repository */
    $repository = \Drupal::service('entity_display.repository');

    $form_display = $repository->getFormDisplay($entity_type, $bundle);
    $form_display->setComponent($name, [
      'type' => 'plupload_file_widget',
      'settings' => [],
    ])->save();
  }

  /**
   * Update image field widget to use Plupload.
   *
   * @param string $name
   *   The name of the field.
   * @param string $entity_type
   *   The entity type.
   * @param string $bundle
   *   The bundle.
   */
  protected function setPluploadImageWidget(string $name, string $entity_type, string $bundle) {

    /** @var \Drupal\Core\Entity\EntityDisplayRepositoryInterface $repository */
    $repository = \Drupal::service('entity_display.repository');

    $form_display = $repository->getFormDisplay($entity_type, $bundle);
    $form_display->setComponent($name, [
      'type' => 'plupload_image_widget',
      'settings' => [
        'preview_image_style' => 'thumbnail',
      ],
    ])->save();
  }

  /**
   * Add a predefined file to Plupload upload queue.
   *
   * @param string $path
   *   The path to the file.
   * @param string $selector
   *   The selector of the Plupload widget.
   */
  protected function addFile(string $path, string $selector): void {
    $session = $this->getSession();

    // Add a fake input element.
    $input = <<<JS
      jQuery('$selector').append('<input type=\'file\' name=\'fakefile\'>');
JS;
    $session->evaluateScript($input);

    // Populate the field with uploaded file.
    $session->getPage()->attachFileToField('fakefile', $path);

    // Add to plupload queue.
    $drop = <<<JS
      jQuery('$selector').pluploadQueue().addFile(jQuery('input[name="fakefile"]')[0]);
JS;
    $session->evaluateScript($drop);
  }

}
