(($) => {
  Drupal.plupload_widget = Drupal.plupload_widget || {};

  // Add Plupload events for autoupload and autosubmit.
  Drupal.plupload_widget.filesAddedCallback = (up) => {
    setTimeout(() => {
      up.start();
    }, 100);
  };

  Drupal.plupload_widget.uploadCompleteCallback = (up) => {
    const $this = $(up.settings.container);
    const $container = $this.closest('.form-managed-file');
    // If there is submit_element trigger it.
    const submitElement =
      drupalSettings.plupload[$this.attr('id')].submit_element;
    if (submitElement) {
      // Trigger the upload button.
      const $button = $container.find(submitElement);
      const $event = drupalSettings.ajax[$button.attr('id')].event;
      $button.trigger($event);
      // Now hide the uploader...
      // the ajax throbber will show.
      $(up).hide();
    }
  };
})(jQuery);
